/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:02 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatSubjectvariablesdataVariable extends BaseXnatSubjectvariablesdataVariable {

	public XnatSubjectvariablesdataVariable(ItemI item)
	{
		super(item);
	}

	public XnatSubjectvariablesdataVariable(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatSubjectvariablesdataVariable(UserI user)
	 **/
	public XnatSubjectvariablesdataVariable()
	{}

	public XnatSubjectvariablesdataVariable(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
