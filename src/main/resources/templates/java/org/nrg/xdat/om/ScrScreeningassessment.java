/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:01 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ScrScreeningassessment extends BaseScrScreeningassessment {

	public ScrScreeningassessment(ItemI item)
	{
		super(item);
	}

	public ScrScreeningassessment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseScrScreeningassessment(UserI user)
	 **/
	public ScrScreeningassessment()
	{}

	public ScrScreeningassessment(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
