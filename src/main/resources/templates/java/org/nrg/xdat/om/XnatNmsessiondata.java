/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:03 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatNmsessiondata extends BaseXnatNmsessiondata {

	public XnatNmsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatNmsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatNmsessiondata(UserI user)
	 **/
	public XnatNmsessiondata()
	{}

	public XnatNmsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
