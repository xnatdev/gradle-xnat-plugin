/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:01 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class PipePipelinedetails extends BasePipePipelinedetails {

	public PipePipelinedetails(ItemI item)
	{
		super(item);
	}

	public PipePipelinedetails(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePipePipelinedetails(UserI user)
	 **/
	public PipePipelinedetails()
	{}

	public PipePipelinedetails(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
