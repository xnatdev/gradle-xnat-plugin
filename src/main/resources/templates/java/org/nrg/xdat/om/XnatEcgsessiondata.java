/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:03 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatEcgsessiondata extends BaseXnatEcgsessiondata {

	public XnatEcgsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatEcgsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatEcgsessiondata(UserI user)
	 **/
	public XnatEcgsessiondata()
	{}

	public XnatEcgsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
