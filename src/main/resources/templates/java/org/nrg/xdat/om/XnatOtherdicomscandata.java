/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:03 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatOtherdicomscandata extends BaseXnatOtherdicomscandata {

	public XnatOtherdicomscandata(ItemI item)
	{
		super(item);
	}

	public XnatOtherdicomscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatOtherdicomscandata(UserI user)
	 **/
	public XnatOtherdicomscandata()
	{}

	public XnatOtherdicomscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
