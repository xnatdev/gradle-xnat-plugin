/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:02 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatComputationdata extends BaseXnatComputationdata {

	public XnatComputationdata(ItemI item)
	{
		super(item);
	}

	public XnatComputationdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatComputationdata(UserI user)
	 **/
	public XnatComputationdata()
	{}

	public XnatComputationdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
