/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:01 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class PipePipelinedetailsParameter extends BasePipePipelinedetailsParameter {

	public PipePipelinedetailsParameter(ItemI item)
	{
		super(item);
	}

	public PipePipelinedetailsParameter(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePipePipelinedetailsParameter(UserI user)
	 **/
	public PipePipelinedetailsParameter()
	{}

	public PipePipelinedetailsParameter(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
