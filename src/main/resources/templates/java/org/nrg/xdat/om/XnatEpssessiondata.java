/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:03 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatEpssessiondata extends BaseXnatEpssessiondata {

	public XnatEpssessiondata(ItemI item)
	{
		super(item);
	}

	public XnatEpssessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatEpssessiondata(UserI user)
	 **/
	public XnatEpssessiondata()
	{}

	public XnatEpssessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
