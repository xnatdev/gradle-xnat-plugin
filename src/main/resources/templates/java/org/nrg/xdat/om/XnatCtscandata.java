/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:02 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatCtscandata extends BaseXnatCtscandata {

	public XnatCtscandata(ItemI item)
	{
		super(item);
	}

	public XnatCtscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatCtscandata(UserI user)
	 **/
	public XnatCtscandata()
	{}

	public XnatCtscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
