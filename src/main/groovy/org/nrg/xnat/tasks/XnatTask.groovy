package org.nrg.xnat.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.reflections.Reflections
import org.reflections.scanners.ResourcesScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder

import java.nio.file.Paths
import java.util.regex.Pattern

/*
 * Need to handle these args:
 *
 * <arg value="-xdir"/>
        <arg value="${basedir}"/>
        <arg value="-javaDir"/>
        <arg value="${basedir}/projects/${xdat.project.name}/src/java"/>
        <arg value="-templateDir"/>
        <arg value="${basedir}/projects/${xdat.project.name}/src/base-templates/screens"/>
        <arg value="-project"/>
        <arg value="${xdat.project.name}"/>
        <arg value="-e"/>
        <arg value="ALL"/>
        <arg value="-skipXDAT"/>
        <arg value="true"/>
        <arg value="-displayDocs"/>
        <arg value="true"/>
        <arg value="-dbUrl"/>
        <arg value="${xdat.project.db.connection.string}"/>
        <arg value="-dbUsername"/>
        <arg value="${xdat.project.db.user}"/>
        <arg value="-dbPassword"/>
        <arg value="${xdat.project.db.password}"/>
 */

class XnatTask extends DefaultTask {
    @SuppressWarnings("GroovyUnusedDeclaration")
    @TaskAction
    def process() {
        // def String[] args = ["-xdir", "~"]
        // final GenerateJavaFile worker = new GenerateJavaFile(args);
        //
        def destination = Paths.get(project.projectDir.absolutePath, "src/generated/java")
        destination.toFile().mkdirs()

        def builder = new ConfigurationBuilder()
        builder.setUrls(ClasspathHelper.forPackage("templates.java"))
        builder.setScanners(new ResourcesScanner())

        Reflections reflections = new Reflections(builder)
        Set<String> classes = reflections.getResources(Pattern.compile(".*\\.java"))

        classes.each { clazz ->
            def reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/${clazz}")))
            def output = destination.resolve(clazz - ~/templates\/java\//).toFile()

            if (!output.parentFile.exists()) {
                output.parentFile.mkdirs()
            }
            def writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output, false)))
            reader?.eachLine { line ->
                writer.println line
            }
            reader?.close()
            writer.close()
        }
    }
}
