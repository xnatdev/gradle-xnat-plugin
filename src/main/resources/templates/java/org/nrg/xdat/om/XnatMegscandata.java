/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:03 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatMegscandata extends BaseXnatMegscandata {

	public XnatMegscandata(ItemI item)
	{
		super(item);
	}

	public XnatMegscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatMegscandata(UserI user)
	 **/
	public XnatMegscandata()
	{}

	public XnatMegscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
