/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:02 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatImagescandataShare extends BaseXnatImagescandataShare {

	public XnatImagescandataShare(ItemI item)
	{
		super(item);
	}

	public XnatImagescandataShare(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatImagescandataShare(UserI user)
	 **/
	public XnatImagescandataShare()
	{}

	public XnatImagescandataShare(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
