/*
 * GENERATED FILE
 * Created on Thu Jan 28 18:10:02 UTC 2016
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatDatatypeprotocol extends BaseXnatDatatypeprotocol {

	public XnatDatatypeprotocol(ItemI item)
	{
		super(item);
	}

	public XnatDatatypeprotocol(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatDatatypeprotocol(UserI user)
	 **/
	public XnatDatatypeprotocol()
	{}

	public XnatDatatypeprotocol(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
